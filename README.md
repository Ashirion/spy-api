# Spy Api

## description
This is an API that can create and retrieve spies in a dedicated DB. Based on Laravel and laravel/sanctum.

## Requirements
* php 8.1
* composer
* mysql

## How to install

Choose the directory you want to install the project and run:

`git clone https://gitlab.com/Ashirion/spy-api.git`

to get all project dependencies run:

`composer install`

to install migrations run:

`php artisan migrate` 

to create test data and bearer tokens run:

`php artisan db:seed`

Bearer tokens can be found in
root dir in 
'storage/logs/personal_access_tokens.log'

## Running tests

You can use phpunit via php artisan by running:

`
php artisan test
`

## How to use


You can run in postman by following this link
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/ff693ac563694afef95c?action=collection%2Fimport)

Also you can find the json export of postman in project root directory.

You will need to add to the authentication tab of postman the bearer token found within the persona_access_tokens.log file

Within the collection are three endpoints

* POST api/spies            that is responsible for the creation of a new spy.
* GET api/spies/random    that returns 5 random spies, restricted to 10 per minute.
* GET api/spies/all       that returns all spies, paginated, sort by name,surname,birthdate,deathdate and filtered by age range.

Payloads and more details can be found in the collections provided above.
