<?php
declare(strict_types=1);

namespace Tests;

use App\Services\SpyService;
use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use DatabaseMigrations;

    public function setUp(): void
    {
        $this->createApplication();

        $this->faker = Factory::create();
        $this->expectedSpyFields = [
            'id',
            'name',
            'surname',
            'agency',
            'country_of_operation',
            'birth_date',
            'death_date',
            'created_at',
            'updated_at'
        ];
    }

    public function tearDown(): void
    {
    }


}
