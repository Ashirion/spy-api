<?php
declare(strict_types=1);

namespace Tests\Unit;

use App\Http\Controllers\SpyController;
use App\Http\Requests\CreateSpyRequest;
use App\Models\Spy;
use App\Services\SpyService;
use Database\Factories\SpyFactory;
use Faker\Factory;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateSpyTest extends TestCase
{

     public function setUp(): void
     {
         parent::setUp();

        $text = file_get_contents(config('app.bearer_token_path'));
        $lines = explode("\n",$text);
        $this->bearerToken= $lines[0];
        $this->agencies = config('app.agencies');

     }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @return void
     */
    public function test_createSpySuccess()
    {
        //arrange
        $data = $this->generateRequest();
        $createRequest = $this->CreateSpyRequestFromRequest($data);
        $spyController = new SpyController(app(SpyService::class));


        //act
        $result = $spyController->store($createRequest);
        $decodedResponse = json_decode($result->getContent());
        $actualSpyFields = array_keys((array)$decodedResponse);


        //assert
        foreach($actualSpyFields as $field){
            $this->assertContains($field,$this->expectedSpyFields);
        }
        foreach($this->expectedSpyFields as $expField){
            $this->assertContains($expField,$actualSpyFields);
        }
        $this->assertSame(Response::HTTP_CREATED,$result->getStatusCode());

    }

    public function test_createSpyFailure()
    {
        //arrange
        $data = $this->generateInvalidRequest();
        $this->expectException(HttpResponseException::class);
        $this->CreateSpyRequestFromRequest($data);
    }

    private function CreateSpyRequestFromRequest($data)
    {
        $createRequest = new Request();
        $createRequest->request->add($data);

        return new CreateSpyRequest($createRequest);
    }

    private function generateRequest()
    {
        $this->faker =  Factory::create();
        $birthDate = $this->faker->dateTime('-18 years')->format('Y-m-d');
        $data = [
                'name'=> $this->faker->firstName,
                'surname' => $this->faker->lastName,
                'agency' => $this->faker->randomElement($this->agencies),
                'countryOfOperation' => $this->faker->country,
                'birthDate' =>$birthDate,
                'deathDate'=> $this->faker->dateTimeBetween($birthDate,'now')->format('Y-m-d')
            ];

        return $data;
    }

    private function generateInvalidRequest()
    {
        $this->faker =  Factory::create();
        $birthDate = null;
        $data = [
            'name'=> $this->faker->firstName,
            'surname' => $this->faker->lastName,
            'agency' => $this->faker->word,
            'countryOfOperation' => $this->faker->country,
            'birthDate' =>$birthDate,
            'deathDate'=> null
        ];

        return $data;
    }

}
