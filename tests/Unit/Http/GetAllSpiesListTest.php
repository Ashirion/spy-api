<?php
declare(strict_types=1);


use App\Exceptions\NotAllowedFilterException;
use App\Http\Controllers\SpyController;
use App\Http\Requests\CreateSpyRequest;
use App\Http\Requests\SpyListRequest;
use App\Services\SpyService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class GetAllSpiesListTest extends TestCase
{
    public function test_getAllSpiesListSuccess(){
        //arrange
        $data = $this->generateRequest();
        $spyListRequest = $this->SpyListRequestFromRequest($data);
        $spyController = new SpyController(app(SpyService::class));

        //act
        $response = $spyController->getAll($spyListRequest);
        $decodedResponse = json_decode($response->getContent());
        $actualSpyFields = array_keys((array)$decodedResponse->data[0]);

        //assert
        $this->assertSame($this->expectedSpyFields,$actualSpyFields);
        $this->assertSame(Response::HTTP_OK,$response->getStatusCode());
    }


    public function test_getAllSpiesListFailure(){
        //arrange
        $data = $this->generateInvalidRequest();
        $this->expectExceptionCode(Response::HTTP_BAD_REQUEST);
        $spyListRequest = $this->SpyListRequestFromRequest($data);


        //act
        $spyController = new SpyController(app(SpyService::class));
        $response = $spyController->getAll($spyListRequest);

        //assert
        $this->assertSame(Response::HTTP_BAD_REQUEST,$response->getStatusCode());
    }


    private function generateRequest()
    {
        $data = [
            "sortBy" => [
                ["death_date" => "asc"],
                ["name" => "desc"],
                ["surname" => "desc"],
                ["birth_date" => "desc"]
            ],
            "filters" => [
                ["age" => [18, 50]]
            ]
        ];

        return $data;
    }

    private function generateInvalidRequest()
    {
        $data = [
            "sortBy" => [
                ["death_date" => "asc"],
                ["name" => "desc"],
                ["surname" => "desc"],
                ["birth_date" => "desc"]
            ],
            "filters" => [
                ["mage" => [18, 50]]
            ]
        ];

        return $data;
    }

    private function SpyListRequestFromRequest($data)
    {
        $createRequest = new Request();
        $createRequest->request->add($data);

        return new SpyListRequest($createRequest);
    }
}
