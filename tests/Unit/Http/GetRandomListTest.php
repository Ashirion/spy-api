<?php
declare(strict_types=1);


use App\Http\Controllers\SpyController;
use App\Services\SpyService;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class GetRandomListTest extends TestCase
{
    public function test_getRandomListSuccess(){
        //arrange
        $spyController = new SpyController(app(SpyService::class));
        $expectedSpiesCount = 5;



        //act
        $response = $spyController->getRandomList();
        $decodedResponse = json_decode($response->getContent());
        $actualSpyCount = count($decodedResponse);
        $actualSpyFields = array_keys((array)$decodedResponse[0]);

        //assert
        $this->assertSame($this->expectedSpyFields,$actualSpyFields);
        $this->assertSame($expectedSpiesCount,$actualSpyCount);
        $this->assertSame(Response::HTTP_OK,$response->getStatusCode());
    }


    public function test_getRandomListFailure(){
        //arrange
        $spyController = new SpyController(app(SpyService::class));
        $falseCountOfRandomSpies = 1000;
        //act
        $this->expectExceptionCode(Response::HTTP_NOT_FOUND);
        $response = $spyController->getRandomList($falseCountOfRandomSpies);

        //assert
        $this->assertSame(Response::HTTP_NOT_FOUND,$response->getStatusCode());

    }
}
