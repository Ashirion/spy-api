<?php

use App\Http\Controllers\SpyController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/
Route::group(['prefix' => 'spies'],function(){
    Route::middleware('auth:sanctum')->post('',[SpyController::class,'store']);
    Route::middleware('throttle:10,1')->get('/random',[SpyController::class,'getRandomList']);
    Route::get('/all',[SpyController::class,'getAll']);
});
