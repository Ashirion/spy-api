<?php
declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\JsonResponse;


class CreateSpyRequest extends BaseRequest
{

    public function rules(): array
    {
        $agencies = implode(',', config('app.agencies'));

        return [
            'name' => 'required|string|unique:spies,name',
            'surname' => 'required|string|unique:spies,surname',
            'countryOfOperation' => 'string',
            'birthDate' => 'required|date|before:'.$this->getDeathDate(),
            'deathDate' => 'required|date|after:'.$this->getBirthDate(),
            'agency' => 'string|in:'.$agencies
        ];
    }

    public function validate(): void
    {
        $data = $this->getRequest()->request->all();
        $rules =  $this->rules();
        $validator = \Illuminate\Support\Facades\Validator::make($data,$rules);
        if($validator->fails()){
            $this->failedValidation($validator);
        }
    }

    public function getName(): ?string
    {
        return $this->getRequest()->get('name');
    }

    public function getSurname(): ?string
    {
        return $this->getRequest()->get('surname');
    }

    public function getAgency(): ?string
    {
        return $this->getRequest()->get('agency');
    }

    public function getBirthDate(): ?string
    {
        return $this->getRequest()->get('birthDate');
    }

    public function getDeathDate(): ?string
    {
        return $this->getRequest()->get('deathDate');
    }

    public function getCountryOfOperation(): ?string
    {
        return $this->getRequest()->get('countryOfOperation');
    }

    protected function populate(): void
    {
        // TODO: Implement populate() method.
    }

    public function failedValidation(Validator $validator): HttpResponseException
    {

        throw new HttpResponseException(new JsonResponse($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY));
    }
}
