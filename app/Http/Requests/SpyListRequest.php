<?php
declare(strict_types=1);

namespace App\Http\Requests;

use App\Exceptions\MissingContentException;
use App\Exceptions\NotAllowedFilterException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\JsonResponse;


class SpyListRequest extends BaseRequest
{

    public function rules(): array
    {
        return [
            'sortBy' => 'array',
            'sortBy.*' => 'array',
            'sortBy.*.*' => 'string|in:asc,desc',
            'filters' => 'array'
        ];
    }

    public function validate(): void
    {
        $data = $this->getRequest()->request->all();
        $rules = $this->rules();
        $validator = \Illuminate\Support\Facades\Validator::make($data, $rules);
        if ($validator->fails()) {
            $this->failedValidation($validator);
        }
        $allowedFilters = config('app.allowed_filters');
        $onlyAllowedFilters = [];
        foreach($this->getFilters() as $filter){
            $result = array_intersect(array_keys($filter),$allowedFilters);

            if (count($result) > 0){
                $onlyAllowedFilters[] = $result;
            }
        }
        if(count($onlyAllowedFilters) === 0){
            throw new NotAllowedFilterException();
        }

    }

    public function getSortBy(): array
    {
        return $this->getRequest()->get('sortBy')[0];
    }

    public function getFilters(): array
    {
        return $this->getRequest()->get('filters');
    }


    public function getAgeFilters(): ?array
    {
         foreach($this->getRequest()->get('filters') as $index => $filter){
             $ak = array_keys($filter);
             if(in_array('age',array_keys($filter))){
                 return $filter;
             }
        }
    }

    protected function populate(): void
    {
        // TODO: Implement populate() method.
    }

    public function failedValidation(Validator $validator): HttpResponseException
    {

        throw new HttpResponseException(new JsonResponse($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY));
    }
}
