<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

abstract class BaseRequest
{
    /**
     * @var Request
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->setRequest($request);


        $this->validate();
        $this->populate();
    }

    public function all($keys = null): array
    {
        return [];
    }

    protected function getRequest(): Request
    {
        return $this->request;
    }

    abstract protected function validate(): void;

    abstract protected function populate(): void;

    protected function setRequest(Request $request): void
    {
        $this->request = $request;
    }
}
