<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSpyRequest;

use App\Http\Requests\SpyListRequest;
use App\Services\SpyService;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class SpyController extends Controller
{
    public function __construct(SpyService $service)
    {
        $this->service = $service;
    }


    public function store(CreateSpyRequest $request): JsonResponse
    {
        $spy = $this->service->store($request);

        return new JsonResponse($spy->toArray(),Response::HTTP_CREATED);
    }


    public function getRandomList($limit=5): JsonResponse
    {
        $randomSpies = $this->service->getRandomList($limit);

        return new JsonResponse($randomSpies,Response::HTTP_OK);
    }

    public function getAll(SpyListRequest $request): JsonResponse
    {
        $paginatedSpies = $this->service->getAll($request);

        return new JsonResponse($paginatedSpies,Response::HTTP_OK);
    }
}
