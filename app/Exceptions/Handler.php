<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Illuminate\Http\Response;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        ThrottleRequestsException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
           //
        });
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @throws \Throwable
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function render($request, Throwable $exception)
    {
        $exceptionMapping = [
            ThrottleRequestsException::class => Response::HTTP_TOO_MANY_REQUESTS,
            MissingContentException::class => Response::HTTP_NOT_FOUND,
            NotAllowedFilterException::class => Response::HTTP_BAD_REQUEST,
        ];

        if (
            $exception instanceof ThrottleRequestsException ||
            $exception instanceof MissingContentException ||
            $exception instanceof NotAllowedFilterException

        ) {
            {
                return response()->json(['message' => $exception->getMessage()], $exceptionMapping[$exception::class]);
            }
        }

        return parent::render($request, $exception);
    }
}
