<?php
namespace App\Exceptions;

use Illuminate\Http\Response;
use JetBrains\PhpStorm\Internal\LanguageLevelTypeAware;
use Symfony\Component\Mime\Exception\LogicException;

class MissingContentException extends LogicException
{
    public function __construct($message = "Missing content! Failed to load at least 5 random spies.", $code = Response::HTTP_NOT_FOUND, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
