<?php
namespace App\Exceptions;

use Illuminate\Http\Response;
use Symfony\Component\Mime\Exception\LogicException;

class NotAllowedFilterException extends LogicException
{
    public function __construct($code = Response::HTTP_BAD_REQUEST, \Throwable $previous = null)
    {
        $message = "Not allowed filters used! Allowed filters are:".implode(",",config('app.allowed_filters'));

        parent::__construct($message, $code, $previous);
    }
}
