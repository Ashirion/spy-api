<?php
declare(strict_types=1);

namespace App\Services;

use App\Exceptions\MissingContentException;
use App\Http\Requests\CreateSpyRequest;
use App\Http\Requests\SpyListRequest;
use App\Models\Spy;
use Illuminate\Support\Facades\DB;

class SpyService
{

    public function store(CreateSpyRequest $request):Spy
    {
        $spy = new Spy();
        $spy->name = $request->getName();
        $spy->surname = $request->getSurname();
        $spy->agency = $request->getAgency();
        $spy->country_of_operation = $request->getCountryOfOperation();
        $spy->birth_date = $request->getBirthDate();
        $spy->death_date = $request->getDeathDate();
        $spy->save();

        return $spy;
    }

    public function getRandomList($limit=5): array
    {
        $result = Spy::all()->random(5)->toArray();

        if(empty($result) || count($result)< $limit){
            throw new MissingContentException();
        }

        return $result;
    }

    public function getAll(SpyListRequest $request)
    {
        $filters = $request->getFilters();
        $filtersA = $request->getAgeFilters();
        $ageFilters= $request->getAgeFilters();

        $validSpyIds = [];

            $validSpyIds= $this->filterAgesReturnIds($validSpyIds,$ageFilters['age']);

        $result = (new Spy())->where(function($query) use($filters,$validSpyIds){
            foreach($filters as $filter){
                if(is_array($filter) && array_key_exists('age',$filter)){
                        $query->whereIn('id',$validSpyIds);
                }
            }
        })->ofSort($request->getSortBy())->paginate(10);

        if(empty($result) || count($result) === 0){
            throw new MissingContentException();
        }

        return $result;
    }

    private function getSpyAges()
    {
        return DB::select(
            "SELECT id,birth_date, death_date, TIMESTAMPDIFF(YEAR,birth_date, death_date) AS age FROM spies"
        );
    }

    private function filterAgesReturnIds($validSpyIds,$ageFilters){
        if($ageFilters){
            $ages = $this->getSpyAges();
            foreach($ages as $age){
                if($age->age >= $ageFilters[0] && count($ageFilters)>1 &&  $age->age <=$ageFilters[1]){
                    $validSpyIds[] = $age->id;
                }
            }
        }

        return $validSpyIds;
    }
}
