<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spies', function (Blueprint $table) {
            $table->id();
            $table->string('name',100);
            $table->string('surname',100);
            $table->string('agency',100)->nullable();
            $table->string('country_of_operation',100)->nullable();
            $table->date('birth_date');
            $table->date('death_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spies');
    }
}
