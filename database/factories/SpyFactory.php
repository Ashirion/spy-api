<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class SpyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $birthDate = $this->faker->date('-18 years')->format();

        return [
            'name' => $this->faker->firstName(),
            'surname' => $this->faker->lastName(),
            'agency' => $this->faker->randomElement(config('app.agencies')),
            'country_of_operation' => $this->faker->country(),
            'birth_date' => $birthDate,
            'death_date' => $this->faker->dateTimeBetween($birthDate,'now')->format('Y-m-d')
        ];
    }
}
