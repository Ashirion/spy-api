<?php

namespace Database\Seeders;

use App\Models\Spy;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if(User::count()<1){
            User::factory(2)->create();
        }
        if(Spy::count()<1){
            Spy::factory(20)->create();
        }
        if(DB::table('personal_access_tokens')->count() <2){
            file_put_contents(config('app.bearer_token_path'),'');
            $randomUsers = User::all()->random(2);
            $randomUsers->each(function($randomUser){
                $tokenName = $randomUser->name.'_token';
                $token = $randomUser->createToken($tokenName);
                file_put_contents(config('app.bearer_token_path'),$token->plainTextToken ."\n",FILE_APPEND);
            });

        }


    }
}
